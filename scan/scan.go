package scan

import (
	"../file"
	"../tvdb"
	"../util"
	"fmt"
	tvdbApi "github.com/pioz/tvdb"
	log "github.com/sirupsen/logrus"
	"regexp"
	"sort"
	"strconv"
)

func FromPath(path string, c *tvdbApi.Client) {

	regexpFilePath := regexp.MustCompile("^/?(.+/)*(.+)$")
	filePathArray := regexpFilePath.FindStringSubmatch(path)
	seriesName := filePathArray[len(filePathArray)-1]

	series := tvdb.FindSeriesOrFail(seriesName, c)
	listSeasonsDir, errListSeasonDirs := file.ListAllDirOnlyInDir(path)
	if errListSeasonDirs != nil {
		log.Fatalln(errListSeasonDirs)
	}

	if len(listSeasonsDir) == 0 {
		log.Fatal("There is no Season directory in " + path)
	}
	var allSeasons []*file.SeasonDir
	for _, element := range listSeasonsDir {
		one := file.NewSeasonDirSeason(element, path)
		allSeasons = append(allSeasons, one)
	}

	err2 := c.GetSeriesEpisodes(&series, nil)
	if err2 != nil {
		log.Fatal(err2)
	}

	var missingEpisodes []*tvdbApi.Episode

	errSummary := c.GetSeriesSummary(&series)
	if errSummary != nil {
		log.Fatal(errSummary)
	}

	seasonsFromTvDB := series.Summary.AiredSeasons
	var airedSeason []int

	for _, oneSeasonFromTvDB := range seasonsFromTvDB {
		converted, _ := strconv.Atoi(oneSeasonFromTvDB)
		airedSeason = append(airedSeason, converted)
	}
	sort.Ints(airedSeason)
	if airedSeason[0] == 0 {
		airedSeason = airedSeason[1:] // We don't care about special episodes
	}
	for _, oneSeason := range allSeasons {
		missingEpisodes = append(missingEpisodes, oneSeason.CheckMissingEpisodes(&series)...)
		airedSeason = util.Filter(airedSeason, func(i int) bool {
			return i != oneSeason.SeasonID
		})
	}

	if len(missingEpisodes) > 0 {
		fmt.Println("Missing episodes are :")
		for _, value := range missingEpisodes {
			fmt.Println("* S0" + strconv.Itoa(value.AiredSeason) + "E" + strconv.Itoa(value.AiredEpisodeNumber))
		}
		if len(airedSeason) > 0 {
			fmt.Println("Missing seasons are :")
			for _, value := range airedSeason {
				fmt.Println("* Season " + strconv.Itoa(value))
			}
		}
	} else {
		fmt.Println("No missing episodes")
	}

}
