package args

import (
	"../../file"
	"errors"
	log "github.com/sirupsen/logrus"
	"os"
)

func CheckIfDirExist(path string) (string, error) {
	exists, err := file.Exists(path)
	if !exists {
		err = errors.New("dir " + path + " not found")
	}
	return path, err
}

func getArgByIndex(index int) string {
	if index >= len(os.Args) {
		log.Fatalln("no arg")
	}
	arg := os.Args[index]
	return arg
}

func GetAllArgs() []string {
	args := os.Args[1:]
	return args
}
