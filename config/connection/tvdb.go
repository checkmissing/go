package connection

import (
	"../env"
	"github.com/pioz/tvdb"
)

func ConnectToTVDB() (tvdb.Client, error) {
	env.LoadEnv()

	c := tvdb.Client{
		Apikey:   env.FindEnvVarOrFail("apiKey"),
		Userkey:  env.FindEnvVarOrFail("userKey"),
		Username: env.FindEnvVarOrFail("username"),
		Language: env.FindEnvVarOrFail("language"),
	}

	err := c.Login()
	if err != nil {
		return c, err
	}
	return c, nil
}
