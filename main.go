package main

import (
	"./config/args"
	"./config/connection"
	"./scan"
	"fmt"
	nested "github.com/antonfisher/nested-logrus-formatter"
	log "github.com/sirupsen/logrus"
)

func init() {
	log.SetFormatter(&nested.Formatter{
		ShowFullLevel:   true,
		TimestampFormat: "2006-01-02 15:04:05",
	})
}

func main() {
	c, errConnection := connection.ConnectToTVDB()
	if errConnection != nil {
		log.Fatal(errConnection)
	}

	allArgs := args.GetAllArgs()
	for _, oneArg := range allArgs {
		path, err := args.CheckIfDirExist(oneArg)
		fmt.Println("Scanning " + path)
		if err != nil {
			log.Errorln(err)
		}
		scan.FromPath(path, &c)
	}
}
